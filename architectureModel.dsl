# Language reference https://github.com/structurizr/dsl/blob/master/docs/language-reference.md
# Preview https://structurizr.com/dsl
# Notation https://structurizr.com/help/notation
# C4 model https://c4model.com/
# Examples https://github.com/structurizr/dsl/tree/master/examples
# Doc de uso de structurizr CLI https://github.com/structurizr/cli/blob/master/docs/push.md
# https://www.websequencediagrams.com/

workspace "CENCOR CI CD model" "Modelo de integracion continua y entregas continuas utilizado en CENCOR." {
    !docs doc
    !adrs decisionLog
    model {
        sweExterno = person "Software engineer externo" "Ingeniero de software externo"

        newRelic = softwareSystem "New Relic" "Sistema de monitoreo"
        artifactRepo = softwareSystem "Artifact repo" "Gitlab package registry - Repositorio de artefactos (librerias reutilizables) de CENCOR" "Repository"
        mavenCentral = softwareSystem "Maven Central" "Repositorio publico de dependencias de terceros" "Repository"
        dockerRegistry = softwareSystem "Docker registry" "Registro de imagenes Docker" "Docker hub"
        deploymentPackageRepository = softwareSystem "Gitlab CI Package repo" "Repositorio de paquetes de despliegue administrado por Gitlab CI" "Repository"
        versionControlSystem = softwareSystem "Gitlab" "Sistema de control de versiones basado en Git administrado por Gitlab.com" "Gitlab"
        nvdDatabase = softwareSystem "NVD Database" "Base de datos que contiene las vulnerabilidades de software hospedado en https://nvd.nist.gov" "Repository"
        dependencyTrack = softwareSystem "Dependency Track" "Identifica dependencias con vulnerabilidades" "Dependency Track" {
            -> nvdDatabase "Obtiene dependencias vulnerables" "HTTPS" "Sync"
        }
        qualityGate = softwareSystem "Sonarqube" "Plataforma de evaluación de calidad de código" "Sonarqube"
        enterprise "CENCOR" {
            deployManager = person "Deployment manager" "Responsable de autorizar despliegues"
            swe = person "Software engineer" "Ingeniero de software"
            sweReviewer = person "Software engineer que revisa Merge request" "Ingeniero de software"

            cencorSystem = softwareSystem "Algun sistema del grupo" {
                deployTarget = container "Servicio" "Servicio a desplegar" "Jar|War|Lambda|React"
            }
            cencorci = softwareSystem "CENCOR CI" "Integracion continua (CI)" "" {
                url https://gitlab.com/cencor
                
                empaquetado = container "Package" "Empaquetado" {
                    publicarPaqueteDespliegue = component "Publica paquete de despliegue" {
                        -> deploymentPackageRepository "Publica deployment-package.tar" "" "Sync"
                    }
                    construirPaquete = component "package.sh" "Construir binario a desplegar" {
                        -> versionControlSystem "Obtiene Tag" "SSH" "Sync"
                        -> publicarPaqueteDespliegue
                    }
                    obtenerAgenteMonitoreo = component "Obtener agente de monitoreo" {
                        -> newRelic "wget newrelic.jar" "HTTPS" "Sync"
                        -> construirPaquete
                    }
                }
                autoversionado = container "Autoversionado" {
                    -> versionControlSystem "git tag push" "Shell" "Sync"
                    -> empaquetado
                }
                staticAnalysis = container "Static Analysis" {
                    vulnerableDependenciesAnalysis = component "Dependency check" {
                        -> dependencyTrack "Verifica si las dependencias cuentan con alguna vulnerabilidad a través de dependencyCheck" "HTTPS" "Sync"
                    }
                    sonarqubeAnalysis = component "Sonarqube analysis" {
                        -> qualityGate "Verifica que criterios de calidad de codigo se cumplan" "HTTPS" "Sync"
                        -> vulnerableDependenciesAnalysis
                    }
                    jacoco = component "Java Code Coverage - Jacoco" {
                        -> sonarqubeAnalysis
                    }
                    checkstyle = component "Checkstyle analysis" {
                        -> versionControlSystem "Obtiene estilos de código" "HTTPS" "Sync"
                        -> jacoco
                    }
                    -> autoversionado
                }
                pruebasContruccion = container "Pruebas de construccion" {
                    end2endTests = component "Component tests"
                    contractTests = component "Contract tests" {
                        -> end2endTests
                    }
                    integrationTests = component "Integration tests" {
                        -> contractTests
                    }
                    unitTests = component "Unit tests" {
                        -> integrationTests
                    }
                    -> staticAnalysis
                }
                construccion = container "Construcción" {
                    compilacion = component "Compilacion de codigo"
                    dependenciasPrivadas = component "Obtiene dependencias privadas" {
                        -> artifactRepo "Obtiene dependencias privadas" "HTTPS" "Sync"
                        -> compilacion
                    }
                    dependenciasPublicas = component "Obtiene dependencias publicas" {
                        -> mavenCentral "Obtiene dependencias públicas" "HTTPS" "Sync"
                        -> dependenciasPrivadas
                    }
                    -> pruebasContruccion
                }
                obtencionCodigo = container "Obtencion de codigo" {
                    -> versionControlSystem "Obtiene codigo" "SSH" "Sync"
                    -> construccion
                }
                
                gitlabRunnerServiceCi = container "Agente de CI" "Sevicio (OS) que verifica si existen trabajos pendientes por ejecutar" "Gitlab runner service" {
                    -> versionControlSystem "Pregunta si hay trabajo que realizar" "HTTPS" "Sync"
                    -> dockerRegistry "Obtencion de imagen para construccion" "" "Sync"
                    -> obtencionCodigo "Inicia construccion" "Docker" "Sync"
                }
            }

            #CENCOR CD
            cencorcd = softwareSystem "CENCOR CD" "Entregas continuas (CD)" "" {
                url https://gitlab.com/cencor
                
                despliegue = container "Despliegue" {
                    -> deployTarget "Despliega"
                    inicializaVersionNueva = component "startup.sh" "Inicializa versión nueva" {
                        -> deployTarget
                    }
                    finalizaVersionActual = component "shutdown.sh" "Detiene versión actual" {
                        -> deployTarget
                        -> inicializaVersionNueva
                    }
                    copiaArtefactos = component "deploy.sh" "Copia/envia artefactos" {
                        -> deployTarget
                        -> finalizaVersionActual
                    }
                    
                }

                configuracionParaAmbiente = container "Configura despliegue para ambiente" {
                    reemplazaPlaceholders = component "Reemplaza placeholders por valores del ambiente"
                    obtencionVariablesDeAmbiente = component "Obtiene valores del ambiente" {
                        -> versionControlSystem "Obtiene valores del ambiente" "HTTPS" "Sync"
                        -> reemplazaPlaceholders "sed -i 's#@PLACEHOLDER_NAME@#'$VALUE'#g' targetFile" "Command" "Sync"
                    }

                    -> despliegue
                }
                obtencionPaqueteDespliegue = container "Obtencion de paquete de despliegue" {
                    -> deploymentPackageRepository "Obtención de deployment-package.tar" "" "Sync"
                    -> configuracionParaAmbiente
                }

                gitlabRunnerServiceCd = container "Agente de CD" "Sevicio (OS) que verifica si existen trabajos pendientes por ejecutar" "Gitlab runner service" {
                    -> versionControlSystem "Pregunta si hay trabajo que realizar" "HTTPS" "Sync"
                    -> dockerRegistry "Obtencion de imagen para despliegue" "" "Sync"
                    -> obtencionPaqueteDespliegue "Inicia despliegue" "Docker" "Sync"
                }
            }

            swe -> versionControlSystem "Push code y crea Merge Request" "Git" "Sync"
            sweReviewer -> versionControlSystem "Integra codigo en master/main"
            sweExterno -> versionControlSystem "Push code y crea Merge Request" "Git" "Sync"
            deployManager -> versionControlSystem "Autoriza despliegue" "Gitlab" "Sync"
        }

        deploymentEnvironment "CI" {
            deploymentNode cencorDataCenter "Cencor" {
                deploymentNode vmware "VMWare" {
                    # Agentes de integración continua.
                    deploymentNode ciServer "CI Server" {
                        description 10.12.230.101
                        technology "CentOS"
                        containerInstance gitlabRunnerServiceCi
                        #Descomentar cantidad de instancias cuando structurizr cli para Java lo soporte
                        #instances "4"
                        deploymentNode "Docker" {
                            technology "Docker"
                            containerInstance obtencionCodigo
                            containerInstance construccion
                            containerInstance pruebasContruccion
                            containerInstance staticAnalysis
                            containerInstance autoversionado
                            containerInstance empaquetado
                        }
                    }
                }
            }
            # Sonarqube
            deploymentNode "Quality gate" {
                url https://sonarcloud.io
                description "Sonar cloud static analysis"
                technology "Sonarcloud"
                softwareSystemInstance qualityGate
            }
            # Dependency Track
            deploymentNode "AWS" {
                deploymentNode "EC2" {
                    deploymentNode "AmazonLinux2" {
                        url http://44.206.40.163
                        softwareSystemInstance dependencyTrack
                    }
                }
            }
            
            # Gitlab
            deploymentNode "Gitlab" {
                url https://gitlab.com/cencor
                description "Gitlab CI/CD"
                technology "Gitlab"
                softwareSystemInstance versionControlSystem
                softwareSystemInstance deploymentPackageRepository
                softwareSystemInstance artifactRepo
            }
            #Docker hub
            deploymentNode "Docker hub" {
                url https://hub.docker.com
                softwareSystemInstance dockerRegistry
            }
            # NVD
            deploymentNode "NVD" {
                url https://nvd.nist.gov
                softwareSystemInstance nvdDatabase
            }
            # MavenRepo
            deploymentNode "MavenRepo" {
                url https://mvnrepository.com/
                softwareSystemInstance mavenCentral
            }
        }

        deploymentEnvironment "CD" {
            cdDev = deploymentGroup "CD Dev"
            cdQA = deploymentGroup "CD QA"
            cdPro = deploymentGroup "CD Pro"
            cdCloud = deploymentGroup "CD Cloud"
            
            # Agentes de entregas continuas.
            deploymentNode cencorDataCenter "Cencor" {
                deploymentNode vmware "VMWare" {
                    deploymentNode qavlan "Qa Vlan" {
                        deploymentNode cdServerQA "CD Server QA" {
                            technology "CentOS"
                            containerInstance gitlabRunnerServiceCd cdQA
                            deploymentNode "Docker" {
                                technology "Docker"
                                containerInstance obtencionPaqueteDespliegue cdQA
                                containerInstance configuracionParaAmbiente cdQA
                                containerInstance despliegue cdQA
                            }
                        }
                        deploymentNode targetServerQA "Server QA" {
                            technology "CentOS"
                            containerInstance deployTarget cdQA
                        }
                    }

                    deploymentNode provlan "PRO Vlan" {
                        deploymentNode cdServerPro "CD Server PRO" {
                            technology "CentOS"
                            containerInstance gitlabRunnerServiceCd cdPro
                            deploymentNode "Docker" {
                                technology "Docker"
                                containerInstance obtencionPaqueteDespliegue cdPro
                                containerInstance configuracionParaAmbiente cdPro
                                containerInstance despliegue cdPro
                            }
                        }
                        deploymentNode targetServerPro "Server PRO" {
                            technology "CentOS"
                            containerInstance deployTarget cdPro
                        }
                    }

                    deploymentNode extranet "Extranet Vlan" {
                        deploymentNode cdServerCloud "CD Server Cloud" {
                            technology "CentOS"
                            containerInstance gitlabRunnerServiceCd cdCloud
                            deploymentNode "Docker" {
                                technology "Docker"
                                containerInstance obtencionPaqueteDespliegue cdCloud
                                containerInstance configuracionParaAmbiente cdCloud
                                containerInstance despliegue cdCloud
                            }
                        }
                    }
                }
            }
            
            deploymentNode awsCencor "AWS" {
                deploymentNode anOrganizationUnit "An Organization Unit" {
                    deploymentNode cloudQa "CloudQA" {
                        technology "CentOS"
                        containerInstance deployTarget cdCloud
                    }
                    deploymentNode cloudPro "CloudPRO" {
                        technology "CentOS"
                        containerInstance deployTarget cdCloud
                    }
                }
            }

            # Gitlab
            deploymentNode "Gitlab" {
                url https://gitlab.com/cencor
                softwareSystemInstance versionControlSystem cdDev,cdQA,cdPro,cdCloud
                softwareSystemInstance deploymentPackageRepository cdDev,cdQA,cdPro,cdCloud
            }
        }
    }
         
    views {
        systemLandscape "CiCdLandscape" {
            include *
            exclude sweExterno swe sweReviewer deployManager
            autoLayout tb 1000
        }

        #################### CI ####################
        component construccion {
            include *
            autoLayout lr
        }
        component pruebasContruccion {
            include *
            autoLayout lr
        }
        component staticAnalysis {
            include *
            autoLayout lr 500
        }
        component empaquetado {
            include *
            autoLayout lr
        }
        container cencorci "CencorCi" {
            title "Modelo de integración continua"
            include *
            autoLayout lr
        }
        deployment * "CI" {
            title "Diagrama de despliegue de integración continua(CI)"
            include *
            autoLayout
        }
        dynamic cencorci "CiSequence" {
            title "Secuencia de operaciones realizadas durante integración continua"
            swe -> versionControlSystem
            gitlabRunnerServiceCi -> versionControlSystem
            gitlabRunnerServiceCi -> dockerRegistry
            gitlabRunnerServiceCi -> obtencionCodigo
            obtencionCodigo -> construccion
            construccion -> pruebasContruccion
            pruebasContruccion -> staticAnalysis
            sweReviewer -> versionControlSystem
            gitlabRunnerServiceCi -> versionControlSystem
            gitlabRunnerServiceCi -> dockerRegistry
            gitlabRunnerServiceCi -> obtencionCodigo
            obtencionCodigo -> construccion
            construccion -> pruebasContruccion
            pruebasContruccion -> staticAnalysis
            staticAnalysis -> autoversionado
            autoversionado -> versionControlSystem
            autoversionado -> empaquetado
            empaquetado -> versionControlSystem
            empaquetado -> deploymentPackageRepository
            autoLayout lr 400
        }

        #################### CD ####################
        component configuracionParaAmbiente "ConfiguracionParaAmbiente" {
            include *
            autoLayout
        }
        component despliegue "despliegue" {
            include *
            autoLayout
        }
        container cencorcd "CencorCd" {
            title "Modelo de entregas continua (CD)"
            include *
            autoLayout
        }
        deployment * "CD" {
            title "Diagrama de despliegue de entregas continuas(CD)"
            include *
            autoLayout
        }
        dynamic cencorcd "CdSequence" {
            empaquetado -> deploymentPackageRepository
            gitlabRunnerServiceCd -> versionControlSystem
            gitlabRunnerServiceCd -> dockerRegistry
            gitlabRunnerServiceCd -> obtencionPaqueteDespliegue
            obtencionPaqueteDespliegue -> deploymentPackageRepository
            obtencionPaqueteDespliegue -> configuracionParaAmbiente
            configuracionParaAmbiente -> versionControlSystem
            configuracionParaAmbiente -> despliegue
            despliegue -> deployTarget "despliega en QA"
            deployManager -> versionControlSystem
            gitlabRunnerServiceCd -> versionControlSystem
            gitlabRunnerServiceCd -> dockerRegistry
            gitlabRunnerServiceCd -> obtencionPaqueteDespliegue
            obtencionPaqueteDespliegue -> deploymentPackageRepository
            obtencionPaqueteDespliegue -> configuracionParaAmbiente
            configuracionParaAmbiente -> versionControlSystem
            configuracionParaAmbiente -> despliegue
            despliegue -> deployTarget "despliega en PRO"
            autoLayout lr
        }

        !include https://gitlab.com/cencor/governance/build-config/-/raw/main/structurizr/cencor-styles.dsl
    }
}
