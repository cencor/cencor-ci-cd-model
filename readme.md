# CENCOR CI/CD model

## Decisiones de arquitectura

Las decisiones de arquitectura tomadas se encuentran registradas con ADR (Architecture Decision Records) de acuerdo al formato propuesto por Michael Nygard.

[Como instalar ADR](https://github.com/npryce/adr-tools/blob/master/INSTALL.md)

### 1 Crear directorio de registro de decisiones

`adr init decisionLog`

Esto generará un directorio con nombre `decisionLog` que contiene la primer decisión. Esta primer decisión indica que estamos usando ADRs para registrar las decisiones de arquitectura y coloca un enlace hacia un artículo sobre esta materia elaborado por Michael Nygard.

### 2 Agregar decisiones de arquitectura

`adr new Titulo de la decision`

Por ejemplo: `adr new Persistir todos los instantes de tiempo como Datetime`

Esto genera una nueva decisión de arquitectura, numerada y abre tu editor de elección especificado por la variable de ambiente `EDITOR`

#### Reemplazar una decisión previa

Para crear una nueva decisión que reemplace a una anterior, por ejemplo la 9, utilizamos la opción -s:

`adr new -s 9 Utilizar Java core en lugar de frameworks (Spring & Hibernate) en Lambdas`

Esto creará un nuevo ADR marcado como reemplazo del ADR 9. También cambia el status de ADR 9 para indicar que ha sido reemplazada por este nuevo ADR. Finalmente abre el editor de tu elección.

### Para mayor información, utiliza la ayuda incluída:

`adr help`

## Documentación

Adicional al registro de decisiones de arquitectura, se cuenta con una sección dedicada para documentar el resto de temas relacionados a la arquitectura en la carpeta `doc`. Esta carpeta cuenta con archivos en formato Markdown y recursos adicionales, como imágenes, que puedan ser referenciados en la documentación.

### Atributos de calidad

Se recomienda documentar los atributos de calidad que fueron formando, guíando o modelando la arquitectura.

Existen muchos factores que determinan las cualidades que deben ser proporcionadas en la arquitectura del sistema. Estas cualidades van más allá de la funcionalidad y son las responsables de calificar la calidad de un producto en base a ciertas expectativas o comportamientos del sistema bajo ciertas circunstancias.

Cada una de estas cualidades las capturamos como "Atributos de calidad" que se define como una propiedad del sistema, ya sea medible o verificable, que se utilizará para indicar que tan bien el sistema satisface las necesidades de los distintos interesados (stakeholders) del sistema.

Un requerimiento de atributo de calidad debe ser verificable (testable) y al mismo tiempo no debe presentar ambigüedades. Para esto utilizamos una forma común para especificar todos los requerimientos de atributos de calidad que consta de las siguientes partes:

#### Partes de un atributo de calidad

* **Fuente del estímulo.** Esto es alguna persona, un sistema de cómputo o algún otro actor que genera el estímulo
* **Estímulo.** El estímulo es una condición que requiere una respuesta cuando llega al sistema.
* **Ambiente.** El estímulo ocurre en ciertas condiciones. El sistema puede estar durante operación normal o con una sobrecarga o algún otro estado relevante. Para muchos sistemas una operación normal puede referirse a varias modalidades, por ejemplo, la modalidad de cancelación de ordenes, la modalidad de cierre de día, etc. Para este tipo de sistemas el ambiente debe de especificar la modalidad en la que el sistema se está ejecutando.
* **Artefacto.** Algún artefacto que es estimulado. Esto puede ser un conjunto de sistemas, el sistema completo, alguna parte o partes del sistema.
* **Respuesta.** La respuesta es la actividad ejecutada como resultado de la llegada del estímulo.
* **Medición de la respuesta.** Cuando la respuesta es obtenida, debe ser medible de cierta forma que el requerimiento pueda ser verificado o probado.
	* En un escenario de performance la medición podría ser latencia o capacidad (throughput).
    * Para modificabilidad puede ser el esfuerzo o el tiempo para realizar una modificacion, probarla y desplegarla.
    * Para disponibilidad podría ser el porcentaje de disponibilidad, el tiempo para detectar la falla, el tiempo para corregir la falla, el tiempo o ventana de tiempo donde el sistema no estará disponible.
    * Para seguridad qué tantas partes del sistema están comprometidas cuando un componente o dato fue comprometido. Cuanto tiempo transcurrió desde que se realizó el ataque. Cuántos ataques son contenidos. Cuánto tiempo toma recuperarse de un ataque exitoso. Qué tanta información es vulnerable a un ataque.
    * Para verificabilidad (testability) podría ser el porcentaje de cobertura de las pruebas, el esfuerzo requerido para ejecutar las pruebas, el esfuerzo en tiempo para preparar un ambiente de pruebas, etc.

Con estas secciones ahora podemos definir un escenario y expresarlo como escenario de atributo de calidad. El conjunto de escenarios de atributos de calidad nos llevará a tener los atributos de calidad del sistema.

#### Ejemplo de un atributo de calidad

Por ejemplo, mostramos las partes de un escenario general de disponibilidad:

* Fuente del estímulo: Personas/Hardware/Infraestructura física/ambiente físico
* Estímulo: Omisión, Falla, Tiempos inapropiados, respuesta incorrecta
* Ambiente: Operación normal, startup, shutdown, modalidad de reparación, operación degradada, operación sobrecargada.
* Respuesta: Prevenir que una falla se convierta en una detención total del sistema. Detectar la falla mediante un registro o una notificación. Recuperarse de la falla quizás desahabilitando el origen del evento. Corregir el error. Entrar en una modalidad degradada.
* Medición de la respuesta: El tiempo o el periodo de tiempo en el que el sistema este disponible. El porcentaje de disponibilidad. El tiempo para detectar la falla, El tiempo de reparación, etc.

Una vez identificadas todas o algunas de las partes (Dependiendo del escenario quizás es posible omitir alguna de las partes) podemos derivarlo en un escenario. Po ejemplo:

* El monitor de salud (a través de heartbeats) determina que una de las instancias del servicio de préstamos no responde durante operación normal. El sistema informa al operador, una nueva instancia es creada y continua la operación sin downtime, permitiendo a lo más tener 10 desconexiones (las de la instancia que no responde).

### Diagramas

En muchas ocasiones un diagrama necesita ser acompañado por texto que permita entender de mejor forma el diagrama. Por ejemplo describir con mayor detalle los componentes mostrados en el diagrama, información contextual que permita entender el diagrama, alguna variabilidad, el racional colocado en el diagrama o alguna otra vista o diagrama relacionado.

En este sentido se recomienda documentar los diagramas con la siguiente estructura

#### Representación principal

Contiene un diagrama o una representación no visual que muestre los elementos y sus relaciones en esta vista. Se indica el lenguaje o la notación utilizada. Si no es una notación estándar se indica una referencia sobre la nomenclatura utilizada.

#### Catálogo de elementos

Esta sección puede ser organizada como un diccionario donde cada entrada es un elemento de la representación principal. Para cada elemento se provee información adicional o propiedades que no son apropiadas o no se pueden mostrar (por claridad) en la representación principal

#### Diagrama de contexto

Contiene un diagrama de contexto que gráficamente muestre el alcance de la parte del sistema representada por esta vista. Un diagrama de contexto típicamente muestra esta parte del sistema como uno solo. generalmente destaca por estar al centro como una caja, envuelta por otras cajas que son las entidades externas con las que se relaciona. Las líneas entre las cajas muestran la relación entre esta parte del sistema y las entidades externas.

#### Guía de variabilidad

Describe cualquier mecanismo de variabilidad utilizado en esta parte del sistema junto con el cómo y cuándo (durante construcción, despliegue, ejecución) se pueden realizar estas variaciones. Algunos ejemplos son valores parametrizados, como toggles, banderas, switches, archivos de propiedades, selección entre distintos proveedores de un elemento, configuración de bases de datos, componentes opcionales (como plugins) o cualquier otro archivo de configuración.

#### Racional

Describe todo el racional para cualquier decisión importante o significativa cuyo alcance está limitado a esta vista. Adicionalmente puede indicar cualquier otra alternativa importante que haya sido rechazada. También se pueden encontrar supuestos, restricciones, resultados de análisis y experimentos y requerimientos de arquitectura que puedan afectar esta vista.
Vistas relacionadas

Enlaces hacia una vista padre, una vista hija, alguna vista más refinada. Si es que existen.

### ¿Cómo incrustar diagramas de arquitectura?

Es posible incrustar diagramas de arquitectura ya creados de la siguiente forma:

```
![](embed:MyDiagramKey)
```

Alternativamente si nuestro diagrama se encuentra publico en alguna URL

```
![Texto alterno](https://structurizr.com/share/1/images/context.png)
```

O haciendo referencia a una imagen en este mismo espacio de trabajo

```
![Texto alterno](directorio/nombreImagen.png)
```

### Publicacion hacia structurizr

Actualmente cada cambio que se escriba en master dispara una publicación de forma automática.

En caso de querer hacer una publicación manual entonces desde la raíz de este repositorio ejecutar el siguiente comando:

```
java -jar structurizr-cli-1.4.5.jar push -id {id} -key {key} -secret {secret} -docs doc -adrs decisionLog -workspace architectureModel.dsl
```

Donde 

* {id} Se reemplaza por el id del workspace de structurizr
* {key} Se reemplaza por el API key del workspace
* {secret} Se reemplaza por el secreto del workspace

