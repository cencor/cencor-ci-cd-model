# Quality Attributes

## Reproducibilidad

El sistema de integración continua es capaz de poder reproducir la construcción de un código fuente publicado en el repositorio de código en un tag de Gitlab mediante la herramienta de construcción Gradle o Maven. El artefacto producido será el mismo que se desplegará en cada ambiente, incluyendo producción. Por ejemplo QA y Producción. TODO Mejorar redaccion de este escenario

TODO Colocar un escenario que represente el uso de docker

TODO Colocar otro escenario que represente la creacion de imagenes docker a partir de codigo

## Velocidad

TODO Colocar un escenario que represente la velocidad de CI como las actividades rapidas (pruebas, static analysis, autoversion)

TODO Colocar un escenario que represente la velocidad de obtencion de feedback

TODO Colocar un escenario que represente la velocidad de despliegue

## Simplicidad

TODO Colocar un escenario que represente la simplicidad de CI/CD

## Disponibilidad

TODO Colocar un escenario que represente la disponibilidad

TODO Colocar un escenario donde se permita mantener la disponibilidad de un servicio durante un desploegue (Canary release, blue-green)

## Serverless o Everything as code

TODO Colocar un escenario que represente que se favorece que el sistema de CI no sea administrado por alguien de CENCOR

## Escalabilidad

TODO Colocar un escenario que represente que el sistema de CI puede escalar para satisfacer la alta demanda del proceso de CI por ejemplo

## Trazabilidad

TODO Colocar un escenario que represente la trazabilidad sobre quien hizo un despliegue
